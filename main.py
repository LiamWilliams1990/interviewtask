import re

filename = 'Game_log.txt'
gameLog = open(filename, 'r')

stakeRX = re.findall(r'(Stake: ..)(.+)', gameLog.read())
listStakes = []
for stake in stakeRX:
    listStakes.append(float(stake[1]))
totalStake = sum(listStakes)
gameLog.close()

gameLog = open(filename, 'r')
winningsRX = re.findall(r'(winnings: ..)(.+)', gameLog.read())
listWins = []
for win in winningsRX:
    listWins.append(float(win[1]))
totalWins = sum(listWins)


rtp = totalWins / totalStake * 100
print('Total stakes: £',totalStake)
print('Total winnings: £',totalWins)
print('Return to player: ',rtp,'%')